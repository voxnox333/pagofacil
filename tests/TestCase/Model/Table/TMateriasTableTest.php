<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TMateriasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TMateriasTable Test Case
 */
class TMateriasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TMateriasTable
     */
    public $TMaterias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TMaterias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TMaterias') ? [] : ['className' => TMateriasTable::class];
        $this->TMaterias = TableRegistry::getTableLocator()->get('TMaterias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TMaterias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
