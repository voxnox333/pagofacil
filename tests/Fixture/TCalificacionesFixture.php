<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TCalificacionesFixture
 *
 */
class TCalificacionesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id_t_calificaciones' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'id_t_materias' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_t_usuarios' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'calificacion' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'fecha_registro' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'id_t_materias' => ['type' => 'index', 'columns' => ['id_t_materias'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id_t_calificaciones'], 'length' => []],
            't_calificaciones_ibfk_1' => ['type' => 'foreign', 'columns' => ['id_t_materias'], 'references' => ['t_materias', 'id_t_materias'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            't_calificaciones_ibfk_2' => ['type' => 'foreign', 'columns' => ['id_t_materias'], 'references' => ['t_materias', 'id_t_materias'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id_t_calificaciones' => 1,
                'id_t_materias' => 1,
                'id_t_usuarios' => 1,
                'calificacion' => 1.5,
                'fecha_registro' => '2019-03-22'
            ],
        ];
        parent::init();
    }
}
