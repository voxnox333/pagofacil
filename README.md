# CakePHP

  

## Guia de instalación

 Esta hecho con Cakephp 3.7
 
[https://book.cakephp.org/3.0/es/installation.html](https://book.cakephp.org/3.0/es/installation.html)

## API

**POST**   {url}/alumno/{alumno_id}/calificacion

Request
```
{
	"materia_id":2,
	"calificacion": 9.5
}
```

**PUT**   {url}/alumno/{alumno_id}/calificacion/{calificacion_id}

Request
```
{
	"calificacion": 9.5
}
```


**DELETE**   {url}/alumno/{alumno_id}/calificacion/{calificacion_id}



**GET**   {url}/alumno/{alumno_id}/calificaciones

```
{
    "id_t_usuarios": 1,
    "nombre": "John",
    "ap_paterno": "Dow",
    "ap_materno": "Down",
    "activo": 1,
    "calificaciones": [
        {
            "calificacion": 9.5,
            "materia": "matematicas",
            "fecha_registro": "2019-03-23"
        },
        {
            "calificacion": 7,
            "materia": "programacion I",
            "fecha_registro": "2019-03-23"
        }
    ],
    "promedio": 8.25
}
```