<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TCalificaciones Model
 *
 * @method \App\Model\Entity\TCalificacione get($primaryKey, $options = [])
 * @method \App\Model\Entity\TCalificacione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TCalificacione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TCalificacione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TCalificacione|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TCalificacione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TCalificacione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TCalificacione findOrCreate($search, callable $callback = null, $options = [])
 */
class TCalificacionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('t_calificaciones');
        $this->setDisplayField('id_t_calificaciones');
        $this->setPrimaryKey('id_t_calificaciones');


        $this->belongsTo('TMaterias', [
            'foreignKey' => 'id_t_materias'
        ]);

        $this->belongsTo('TAlumnos', [
            'foreignKey' => 'id_t_usuarios'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_t_calificaciones')
            ->notEmpty('id_t_calificaciones', 'create');

        $validator
            ->integer('id_t_materias')
            ->requirePresence('id_t_materias', 'create')
            ->notEmpty('id_t_materias', false);

        $validator
            ->integer('id_t_usuarios')
            ->requirePresence('id_t_usuarios', 'create')
            ->notEmpty('id_t_usuarios', false);

        $validator
            ->decimal('calificacion')
            ->notEmpty('calificacion');

        $validator
            ->date('fecha_registro')
            ->notEmpty('fecha_registro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id_t_calificaciones']));
        $rules->add($rules->existsIn(['id_t_materias'], 'TMaterias'));
        $rules->add($rules->existsIn(['id_t_usuarios'], 'TAlumnos'));
        return $rules;
    }

}
