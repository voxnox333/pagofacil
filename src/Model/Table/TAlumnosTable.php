<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TAlumnos Model
 *
 * @method \App\Model\Entity\TAlumno get($primaryKey, $options = [])
 * @method \App\Model\Entity\TAlumno newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TAlumno[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TAlumno|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TAlumno|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TAlumno patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TAlumno[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TAlumno findOrCreate($search, callable $callback = null, $options = [])
 */
class TAlumnosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('t_alumnos');
        $this->setDisplayField('id_t_usuarios');
        $this->setPrimaryKey('id_t_usuarios');

        $this->hasMany('TCalificaciones', [
            'foreignKey' => 'id_t_usuarios'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_t_usuarios')
            ->notEmpty('id_t_usuarios', 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 80)
            ->notEmpty('nombre');

        $validator
            ->scalar('ap_paterno')
            ->maxLength('ap_paterno', 80)
            ->notEmpty('ap_paterno');

        $validator
            ->scalar('ap_materno')
            ->maxLength('ap_materno', 80)
            ->notEmpty('ap_materno');

        $validator
            ->integer('activo')
            ->notEmpty('activo');

        return $validator;
    }

    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id_t_usuarios']));

        return $rules;
    }
}
