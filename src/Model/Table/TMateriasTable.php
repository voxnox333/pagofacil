<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TMaterias Model
 *
 * @method \App\Model\Entity\TMateria get($primaryKey, $options = [])
 * @method \App\Model\Entity\TMateria newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TMateria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TMateria|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TMateria|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TMateria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TMateria[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TMateria findOrCreate($search, callable $callback = null, $options = [])
 */
class TMateriasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('t_materias');
        $this->setDisplayField('id_t_materias');
        $this->setPrimaryKey('id_t_materias');

        $this->hasMany('TCalificaciones', [
            'foreignKey' => 'id_t_materias'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_t_materias')
            ->notEmpty('id_t_materias', 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 80)
            ->notEmpty('nombre');

        $validator
            ->integer('activo')
            ->notEmpty('activo');

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id_t_materias']));

        return $rules;
    }

}
