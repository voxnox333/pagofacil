<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TCalificacione Entity
 *
 * @property int $id_t_calificaciones
 * @property int $id_t_materias
 * @property int $id_t_usuarios
 * @property float|null $calificacion
 * @property \Cake\I18n\FrozenDate|null $fecha_registro
 */
class TCalificacione extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id_t_materias' => true,
        'id_t_usuarios' => true,
        'calificacion' => true,
        'fecha_registro' => true
    ];
}
