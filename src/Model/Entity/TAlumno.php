<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TAlumno Entity
 *
 * @property int $id_t_usuarios
 * @property string|null $nombre
 * @property string|null $ap_paterno
 * @property string|null $ap_materno
 * @property int|null $activo
 */
class TAlumno extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'ap_paterno' => true,
        'ap_materno' => true,
        'activo' => true
    ];
}
