<?php

namespace App\Controller\API\V1;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;
use Cake\Core\Exception\Exception ;
use Cake\Http\Exception\NotFoundException;

class AlumnoController extends AppController
{

     function initialize()
    {
        parent::initialize();
        $this->loadModel('TAlumnos');
        $this->loadModel('TCalificaciones');
    }

     public function calificacion($alumno_id,$cali_id=null){

        $this->request->allowMethod(['POST','PUT',"DELETE"]);

        
        if ( $this->request->is('POST') ) {


            $data = $this->request->getData();

            $data['alumno_id']= $alumno_id;
            $result = $this->add($data);
           
        }else if( $this->request->is('PUT') ){

            $data = $this->request->getData();

            $data["alumno_id"]=$alumno_id;
            $data["cali_id"]=$cali_id;
            $result = $this->modify($data);
        
        }else{
            $data["cali_id"]=$cali_id;

            $result = $this->delete($data);
        }
        
        
        $this->set($result);

    }
   

    private function existcali ($data){

        $exist = $this->TCalificaciones->find()
        ->select(['id_t_calificaciones'])
        ->where(['id_t_materias'=>$data['materia_id'],'id_t_usuarios'=>$data["alumno_id"]]);

        return $exist;
    }


    private function add($data){


        if( !isset($data['materia_id']) || empty($data['materia_id']) || 
            !isset($data['calificacion']) || empty($data['calificacion']) ) {
            throw new BadRequestException('Faltan datos');
        }

        if( !is_integer($data['materia_id']) || !is_numeric($data['calificacion']) ) {
            throw new BadRequestException('El formato de los datos son incorrectos');
        }

        if( !($data['calificacion']>=0 && $data['calificacion']<=10) ){
            throw new BadRequestException('La calificacíon debe estar en el rango de 0 al 10');
        }

        $Cali = $this->existcali($data);


        $result = [
            "success"=>true,
            "msg"=>"calificacion registrada"
        ];

        if (!$Cali->isEmpty()){
            $result = [
                "success"=>false,
                "msg"=>"Existe registro de la calificación"
            ];
            return $result;
        }

        $Cali = $this->TCalificaciones->newEntity();

        $now = date ('Y-m-d');
        $Cali = $this->TCalificaciones->patchEntity($Cali,[
                "id_t_materias"=>$data['materia_id'],
                "id_t_usuarios"=>$data['alumno_id'],
                "calificacion"=>$data['calificacion'],
                "fecha_registro"=> $now 
            ]
        );

        if (!$this->TCalificaciones->save($Cali)) {
            if($Cali->getErrors()){
                $errorMessage = json_encode($Cali->getErrors());
                throw new BadRequestException($errorMessage);
            }
            throw new BadRequestException(__('No se pudo guardar la calificación por un error desconocido'));
        }

        return $result;
    }


    private function modify($data){

        if( !isset($data['calificacion']) || empty($data['calificacion']) ) {
            throw new BadRequestException('Faltan datos');
        }

        if( !is_numeric($data['calificacion']) ) {
                throw new BadRequestException('El formato de los datos son incorrectos');
        }

        if( !($data['calificacion']>=0 && $data['calificacion']<=10) ){
            throw new BadRequestException('La calificacíon debe estar en el rango de 0 al 10');
        }

        $result = [
            "success"=>true,
            "msg"=>"calificacion actualizada"
        ];

        try {
            $Cali = $this->TCalificaciones->get( $data['cali_id'] );
        } catch (Exception $e) {
            $result = [
                "success"=>false,
                "msg"=>"No existe calificacíón que modificar"
            ];
            return $result;
        } 

        $Cali->calificacion= $data['calificacion'];

        if (!$this->TCalificaciones->save($Cali)) {
            if($Cali->getErrors()){
                $errorMessage = json_encode($Cali->getErrors());
                throw new BadRequestException($errorMessage);
            }
            throw new BadRequestException(__('No se pudo guardar la calificación por un error desconocido'));
        }

        return $result;
    }


    private function delete($data){


        $result = [
            "success"=>true,
            "msg"=>"La calificación fue borrada correctamente"
        ];

        try {
            $Cali = $this->TCalificaciones->get( $data['cali_id'] );
        } catch (Exception $e) {
            $result = [
                "success"=>false,
                "msg"=>"No existe calificacíón que modificar"
            ];
            return $result;
        } 

        $this->TCalificaciones->delete($Cali);

        return $result;
    }

    public function calificaciones($alumno_id){

        $this->request->allowMethod(['GET']);
        
        $alumno = $this->TAlumnos->find()->hydrate(false)->select([
            'id_t_usuarios',
            'nombre','ap_paterno','ap_materno',
            'activo'
        ])
        ->contain([
            'TCalificaciones'=>[
                'fields'=>[
                    'TCalificaciones.id_t_usuarios',
                    'calificacion'=>'TCalificaciones.calificacion',
                    'materia'=>'TMaterias.nombre',
                    'fecha_registro_f'=>'DATE_FORMAT(TCalificaciones.fecha_registro, "%Y-%m-%d") '
                ],
                'TMaterias'
            ]
        ])->where(["TAlumnos.id_t_usuarios"=>$alumno_id]);
        

        $result = [];

        if ( $alumno->isEmpty() ){
            throw new NotFoundException("No existe el Usuario");
        }else{
            $datAlumno = $alumno->toArray();

            $promedio = 0;
            $num_mat =0;
            
            $result = $datAlumno[0];

            foreach($result["t_calificaciones"] as &$cali){
                
                $cali["fecha_registro"] = $cali["fecha_registro_f"];
                unset($cali["id_t_usuarios"]);
                unset($cali["fecha_registro_f"]);
                $promedio+=$cali["calificacion"];
                $num_mat++;

            }

            $result["calificaciones"] = $result["t_calificaciones"];
            unset($result["t_calificaciones"]);
            
            $result["promedio"]= +number_format($promedio/$num_mat, 2, '.', '');


        }
       

        $this->set($result);

    }
}
